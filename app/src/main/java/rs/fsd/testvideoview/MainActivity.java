package rs.fsd.testvideoview;

import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;
import android.widget.VideoView;

public class MainActivity extends AppCompatActivity implements MediaPlayer.OnCompletionListener {

    private boolean playingFirst;
    private VideoView videoView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        videoView = findViewById(R.id.video_view);
        videoView.setOnCompletionListener(this);
        videoView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                videoView.stopPlayback();
                videoView.setVisibility(View.GONE);
                return true;
            }
        });

        Uri video = Uri.parse("android.resource://rs.fsd.testvideoview/raw/video1");
        playingFirst = true;
        videoView.setVideoURI(video);
        videoView.start();
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        Toast.makeText(this, "Video complete!", Toast.LENGTH_LONG).show();

        Uri video;
        if(playingFirst)
            video = Uri.parse("android.resource://rs.fsd.testvideoview/raw/video2");
        else
            video = Uri.parse("android.resource://rs.fsd.testvideoview/raw/video1");

        playingFirst = !playingFirst;

        videoView.setVideoURI(video);
        videoView.start();
    }

}
